using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TESTS
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TrueIsTrueTest()
        {
            Assert.IsTrue(true, "True should always be true");
        }
        [TestMethod]
        public void EqualityTest()
        {
            string val = "1";
            Assert.AreEqual("1", val, "One should equal one");
        }
        [TestMethod]
        public void NullTest()
        {
            string val = null;
            Assert.IsNull(val, "Value should be null");
        }
    }
}
